import click

from src.model import model
from src.recurse import recurse
from src.utils.functions import isfolder


@click.group()
def cli():
    """
    group for all commands
    
    :command: recurse
              -> build a path to all images to process
              :option -f|--folder: the relative path to the image folder. 
              images must be in the `src/statics' dir.
    
    :command model: 
             -> create a model for the images listed by the `recurse' command
             :option -f|--folder: the folder to be processed, from the project's 
                                  root. the folder must be in `src/static/``
    """


@cli.command("recurse")
@click.option("-f", "--folder"
              , help="relative path to the images to be processed in "
                     + "the src/statics/ directory, from the `src/statics/` directory")
def cli_recurse(folder):
    """
    build a list of image files from a folder and its subfolders.
    
    :param folder: the folder
    """
    isfolder(folder)
    recurse(folder)
    return


@cli.command("model")
@click.option("-f", "--folder"
              , help="relative path to the images to be processed in "
                     + "the src/statics/ directory, from the `src/statics/` directory")
def cli_model(folder):
    """
    train the clip model on the images in `folder`.
    
    :param folder: the folder to be processed, from the project's root. the folder must be in static/
    """
    isfolder(folder)
    model(folder)


if __name__ == "__main__":
    cli()






