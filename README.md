# RICHELIEU CLIP

### 🍴Forked from [Jean-Philippe Moreux's CLIP test](https://github.com/altomator/CLIP_test), itself a fork from [@ThorkildFregi's app](https://github.com/ThorkildFregi/CLIP-model-website) (developped @ Bibliothèque nationale de France).

RICHELIEU CLIP is a small Flask app to run natural language queries (in english) on the Richelieu 
image set, using OpenAI's zero-shot image classification model, [CLIP](https://openai.com/research/clip)
(source on [Github](https://github.com/openai/CLIP), [paper](https://arxiv.org/abs/2103.00020)).

---

## Installation and use

```bash
# install
git clone https://gitlab.inha.fr/snr/rich.data/richelieu_clip.git
cd richelieu_clip

# env creation
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt  # might need to tweak the installs to fit your hardware: cpu, gpu...

# running, given that you have a `src/statics/richelieu` folder with images to process
python cli_prep.py recurse -f richelieu  # list the files to process
python cli_prep.py model -f richelieu    # build the model
python cli_app.py -f richelieu           # launch the app
```

---

# OLD README 
This toy web app is leveraging the text-image CLIP model and a Flask web app coded by [ThorkildFregi](https://github.com/ThorkildFregi/CLIP-model-website).

*Dependencies: flask, clip, PIL, torch, numpy*



First, launch:
```
>python3 recurse.py -f static/myImages > static/myImages_directory.txt
```
if you want to process a folder of images named ``myImages`` (subfolders may be used within this folder) stored in the ``static`` application folder. It will generate the directory files list and a report as both text files.

Then the CLIP embeddings are computed with:
```
>python3 model.py -f myImages
```
The embeddings are saved in a Torch tensor named after the folder name (in our example, ``myImages_torch.pt``).

Finally, launch the web app:
```
>flask --app main.py --debug run
```
and open this URL http://127.0.0.1:5000 in your browser.

Note: the whole workflow can be run using the bash script ``run.sh``.

The web app displays a random selection of images and a prompt field.

![The web app](screen/home.png)

## Classification scenario

For this use case, we want to use CLIP as a zero-shot classifier. The images types (classes) we want to classify are described in the myImages_labels.csv file as textual captions, e.g.:
```
photo, a photo
map,a map
crossword,a crossword grid
drawing,a drawing
...
```
The results list shows the most likely images for the requested class, the associated probability and the second most likely class. In this example, we are looking for crossword grids in newspapers.

![Classification](screen/classify.png)

## Information retrieval scenario

In this scenario, a free-form query is entered and the result list displays the images ranked in order of probability. In the example below, we are looking for cartoons of people in the street.

![Classification](screen/CBIR.png)


