import click
import sys
import os

from src.utils.functions import path2images, pathisdir, isfolder


@click.command()
@click.option("-f", "--folder"
              , help="relative path to the images to be processed in "
                     + "the src/statics/ directory, from the `src/statics/` directory")
def cli_app(folder):
    """
    run the click app using the images in `folder`
    
    :param folder: the image directory to process, in the `src/statics` dir,
                   from the `src/statics` directory
    
    """
    # check the filepath and save it
    folder = isfolder(folder)
    path2images(folder)
    
    if not pathisdir():
        print("the folder given with `-f` does not exist: `folder`")
        sys.exit(1)
    
    # avoid import errors and run the app
    from src.app import app
    app.run(debug=True)
    return


if __name__ == "__main__":
    cli_app()



