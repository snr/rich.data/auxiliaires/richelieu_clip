# > flask --app main.py --debug run
# puis http://127.0.0.1:5000

from flask import Flask, request, render_template
import random
import torch
import clip

from .utils.functions import full2web
from .utils.clip_config import ClipConfig
from .utils.constants import TEMPLATES, STATICS


# ************************************************************
# flask app to perform a natural language search on
# a set of images from the richelieu project and display 
# the results on a webpage.
#
# DOCS / TUTORIALS:
# * https://github.com/rom1504/clip-retrieval
# * https://github.com/altomator/CLIP_test
# ************************************************************


# configure our clip app and run it
clip_config = ClipConfig()
app = Flask(
    __name__
    , template_folder=TEMPLATES
    , static_folder=STATICS
)

# flask route
@app.route('/', methods=("POST", "GET"))
def page():
    
    # ********************** POST REQUEST ********************* #
    if request.method == "POST":
        print('...receiving POST request')

        clip.tokenize("Hello world!")
        clip_config.preprocess

        query = request.form["prompt"]
       
        # 1ST USE CASE: PRE-CLASSIFIED
        if query in clip_config.labels:
            minProb = 1.0/len(clip_config.labels)  # minProb should be > random guess
            print(f"\n-----------------\n threshold prob for classification: {minProb:.2f}")
            
            # the label's index of the targeted class
            label_index = clip_config.labels.index(query)
            
            # the caption of the targeted class
            caption = clip_config.captions[label_index]
            
            # (?) compare the image against the predefined image labels 
            print("#################")
            print(caption)
            text_descriptions = [ f"{label}" for label in clip_config.captions ]
            text_tokens = clip.tokenize(text_descriptions)
            with torch.no_grad():
                text_features = clip_config.model.encode_text(text_tokens).float()
                text_features /= text_features.norm(dim=-1, keepdim=True)
            text_probs = (100.0 * clip_config.image_features @ text_features.T).softmax(dim=-1)

            # we're looking for the top #2 probs
            top_probs, top_labels = text_probs.cpu().topk(2, dim=-1)
            print("####### top probs for the top 2 classes [10] ##########")
            print(top_probs[:10])
            print(top_labels[:10])

            # filtering the images on a probability threshold for the targeted class
            nameImageTopProb = []
            prob = []
            tuples = []
            errs = []  # save the indexes for which we have an error
            for i, img_path in enumerate(clip_config.image_paths):
                # here, there's an error because it can't make the comparison between
                # a tensor (`top_labels[i][0]` and integers > `2230`. idk y tho
                
                # orig: if ( top_labels[i][0] == label_index ):
                # if (float(top_probs[i][0]) > minProb and top_labels[i][0]==label_index):
                try:
                    if ( top_labels[i][0] == label_index ):
                        tuples.append((
                            img_path
                            , float(top_probs[i][0])
                            , top_labels[i][1]
                            , float(top_probs[i][1])
                        ))
                except IndexError:
                    errs.append(i)
            print(errs[0], errs[-1], len(errs))
            print("  number of images validating the query: " + str(len(tuples)))
            
            # sorting the probs by decreasing values
            decreasing = sorted(tuples, key=lambda criteria: criteria[1], reverse=True)
            
            # build the arrays for the HTML rendition: max_results first items
            nameImageTopProb = [ i[0] for i in decreasing[:clip_config.max_results] ]
            print("####### files for the result images ##########")
            print(nameImageTopProb)
            prob1 = [ i[1] for i in decreasing[:clip_config.max_results] ]
            stringProb1 = ["%.3f" % number for number in prob1]
            print("####### top prob #1 for the targeted class ##########")
            print(stringProb1)
            
            # the second best class
            prob2 = [ i[3] for i in decreasing[:clip_config.max_results] ]
            stringProb2 = [ "%.3f" % number for number in prob2 ]
            class2 = [ clip_config.labels[i[2]] 
                       for i in decreasing[:clip_config.max_results] ]
            print("####### top prob #2 for the result images ##########")
            print(stringProb2)
            print(class2)
            
            return render_template(
                "grid_classif.html"
                , files=full2web(nameImageTopProb)
                , prob1=stringProb1
                , targetClass=query
                , class2=class2
                , prob2=stringProb2
                , query=query
                , caption=caption
                , comment="("+str(len(tuples)) + " results, first "+str(clip_config.max_results)+" displayed)"
            )
            
        # 2ND USE CASE: IMAGE RETRIEVAL WITHOUT PRE-CLASSIFICATION
        else:
            # base variables
            text_descriptions = []
            query = request.form["prompt"]
            text_descriptions.append(f""+query)
            print(text_descriptions)
            
            # computer vision stuff
            text_tokens = clip.tokenize(text_descriptions)
            with torch.no_grad():
                text_features = clip_config.model.encode_text(text_tokens).float()
                text_features /= text_features.norm(dim=-1, keepdim=True)

            text_probs = (clip_config.image_features @ text_features.T)
            
            # sort the results
            sorted_values, indices = torch.sort( text_probs, dim=0, descending=True )
            nameImageTopProb = [
                clip_config.image_paths[i] 
                for i in indices[:clip_config.max_results]
            ]
            # print(nameImageTopProb)  # most probable file results
            prob = sorted_values[:clip_config.max_results]
            stringProb = [ "%.3f" % number for number in prob ]
            
            print(stringProb)
            
            return render_template(
                "grid_cbir.html"
                , files=full2web(nameImageTopProb)
                , prob=stringProb
                , query=query
                , comment="(first "+str(clip_config.max_results)+" results displayed)"
            )
    
    # ********************** GET REQUEST ********************* #
    # building the home page: images mosaic
    else:
        print('...Starting to populate the image grid')
        print('   from folder: '+ clip_config.image_folder)

        random_files = []
        i = 0
        n = 0
        r = int( clip_config.image_count / clip_config.max_thumbnails )
        
        # randomly select `max_thumbnails` images on the whole dataset
        for filename in clip_config.image_paths:
            n+=1
            rdm = random.randint(0, r)
            if rdm == r:
                filename = filename[:-1] # chop the last char = return
                random_files.append(filename)
                i+=1
            if i >= clip_config.max_thumbnails:
                break
        print(f"   files: {n}")
        
        return render_template(
            "home.html"
            , files=full2web(random_files)
            , classes=', '.join(clip_config.labels)
            , count=clip_config.image_count
            , summary=clip_config.summary
        )
    

