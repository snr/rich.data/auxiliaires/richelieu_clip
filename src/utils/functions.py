from flask import url_for
import sys
import os
import re

from .constants import STATICS, UTILS


def path2images(folder: str) -> None:
    """
    build an absolute path to the images folder 
    and save it to UTILS/folder.txt
    
    :param folder: absolute path to the images
    """
    with open(os.path.join(UTILS, "folder.txt"), mode="w") as fh:
        fh.write(folder)
    return


def isfolder(folder) -> None:
    """
    used by several cli commands. check that the folder
    given by the `-f|--folder` command points to an actual
    folder.
    if `folder` does not exist, show an error image and exit 
    
    :param folder: the folder to process, as given by the cli user
    :returns: the full path to the folder 
    """
    _folder = folder  # save the old folder
    if not folder or folder == STATICS:
        print("no folder was given ! exiting. provide one with the `-f` argument.")
        sys.exit(1)
    
    folder = os.path.abspath(os.path.join(STATICS, folder))
    if not os.path.isdir(folder):
        print(f"folder {_folder} does not exist !"
              + "\nfull path: {folder}"
              + "\nexiting...")
        sys.exit(1)
    
    return folder


def full2web(fpaths: list) -> list:
    """
    from a list of absolute filepaths,
    build a list of urls to those same images
    """
    return [ 
        url_for("static"
                , filename=re.sub( "^/", "", f.replace(STATICS, "").replace("\n", ""))
        ) 
        for f in fpaths 
    ]


# UNSUED
def pathisdir() -> bool:
    """
    check that the folder given by the user exists.
    """
    with open(os.path.join(UTILS, "folder.txt"), mode="r") as fh:
        path = fh.read()
    return os.path.isdir(path)

