import torch
import clip
import csv
import os

from .constants import UTILS


# ************************************************************************ #
# create the configuration data for CLIP, torch and for the app.
# create a `ClipConfig` object that is accessible to the flask app
# and to all flask routes to be able to use this data.
# ************************************************************************ #


def config():
    """
    configure click
    """
    # get the targeted folder from a log file
    with open(os.path.join(UTILS, "folder.txt")) as fh:
        image_folder = fh.read()  # full path to the directory storing the images
    
    print('...working on image folder: ' + image_folder)
    
    print('...loading the CLIP model')
    model, preprocess = clip.load("ViT-B/32")
    model.eval()
    # input_resolution = model.visual.input_resolution
    # context_length = model.context_length
    # vocab_size = model.vocab_size
    
    # tensor
    tensor_file = os.path.join( UTILS, f"{os.path.basename(image_folder)}_torch.pt")
    if not os.path.exists(tensor_file):
        print("### Torch tensor is missing: " + tensor_file)
        quit()
    print("...loading the embeddings from: " + tensor_file)
    image_features = torch.load(tensor_file)
    
    # load the files
    if not os.path.exists(image_folder):
        print("### image folder does not exist: " + image_folder)
        quit()
    directory_file = image_folder + ".txt"  # `statics/richelieu.txt`
    print("...reading the directory list: " + directory_file)
    if not os.path.exists(directory_file):
        print("### directory list does not exist: " + directory_file)
        quit()
    with open(directory_file) as f:
        image_paths = f.readlines()
    image_count = len(image_paths)
    print("   number of images found: " + str(image_count))
    
    # load the directory summary
    directory_summary = image_folder + "_directory.txt"  # `statics/richelieu_directory.txt`
    if not os.path.exists(directory_summary):
        print("### directory summary does not exist: " + directory_summary)
        quit()
    with open(directory_summary) as f:
        summary = f.read()
    
    max_results= 100      # max number of displayed results
    max_thumbnails = 200  # max displayed thumbnails on the home page
    
    # load the predefined classes for the classification scenario
    labels = []
    captions = []
    labels_file = image_folder + "_labels.csv"
    if not os.path.exists(labels_file):
        print("### csv labels file is missing: " + labels_file)
        quit()
    with open(labels_file) as f:
        label = csv.reader(f, delimiter=',')
        for row in label:
            labels.append(row[0])
            captions.append(row[1])
    print(labels)
    
    return (
        model
        , preprocess
        , labels
        , captions
        , summary
        , image_features
        , image_paths
        , image_folder
        , image_count
        , max_results
        , max_thumbnails
    )


class ClipConfig():
    """
    class to hold all of our data
    """
    def __init__(self):
        """
        initialise our clipconfig object
        """
        (
            self.model
            , self.preprocess
            , self.labels
            , self.captions
            , self.summary
            , self.image_features
            , self.image_paths
            , self.image_folder
            , self.image_count
            , self.max_results
            , self.max_thumbnails
        ) = config()
        return


