import os

UTILS = os.path.abspath(os.path.dirname(__file__))
SRC = os.path.abspath(os.path.join(UTILS, os.pardir))
ROOT = os.path.abspath(os.path.join(SRC, os.pardir))
TEMPLATES = os.path.abspath(os.path.join(SRC, "templates"))
STATICS = os.path.abspath(os.path.join(SRC, "statics"))