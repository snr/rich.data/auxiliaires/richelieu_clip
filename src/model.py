# Generates the CLIP embeddings for a list of image files
# output: Torch tensor
# input: folder name
# prerequisite: directory list of the target folder

from PIL import Image, UnidentifiedImageError
from PIL.Image import DecompressionBombError
import numpy as np
import torch
import clip
import sys
import os

from .utils.constants import STATICS, UTILS


def model(folder: str):
    """
    pipeline function.
    all images are processed by CLIP. if there's an error
    of if the files are thumbnails (contain `_thumbnail` in the filename),
    they are moved to different folders
    
    :param folder: the folder in `statics` to process
    """
    # ************* DEFINE NONSTANDARD OUTPUT DIRECTORIES ************* #
    unprocessed = os.path.join(STATICS, folder, "_unprocessed")
    thumbnail = os.path.join(STATICS, folder, "_thumbnail")
    if not os.path.isdir(unprocessed):
        os.makedirs(unprocessed)
    if not os.path.isdir(thumbnail):
        os.makedirs(thumbnail)
            
    # ************* EXTRACT THE FILES TO PROCESS ************* #
    print("...reading directory list for: ", folder)
    directory_file = os.path.join(STATICS, f"{folder}.txt")
    if not os.path.exists(directory_file):
        print("directory list {directory_file} for {folder} does not exist!")
        sys.exit(1)
    
    with open(directory_file) as fh:
        image_paths = [ f[:-1] for f in fh.readlines() ]  # remove the `\n` at the end of the line
    image_count = len(image_paths)
    
    # at the end of the process, replace the contents of `directory_file` 
    # by the list of the files which were not moved to `_unprocessed` or `_thumbnail`
    image_paths_kept = image_paths
    
    # ************* PREPARE THE CLIP MODEL ************* #
    print("...loading the CLIP model")
    print("   Torch version:", torch.__version__)
    
    # load our model from clip
    clip.available_models()
    model, preprocess = clip.load("ViT-B/32")
    model.eval()
    input_resolution = model.visual.input_resolution
    context_length = model.context_length
    vocab_size = model.vocab_size
    
    print("   model parameters: " 
          +   f"{np.sum([int(np.prod(p.shape)) for p in model.parameters()]):,}")
    print("   input resolution:", input_resolution)
    print("   context length:", context_length)
    print("   vocab size:", vocab_size)
    print("")
    print(f"   image set size: {image_count} images")
    
    # image preprocessing
    # the second return value from clip.load() contains 
    # a torchvision Transform that performs this preprocessing.
    preprocess
    
    # text preprocessing
    clip.tokenize("Hello world!")
    
    # ************* PROCESS ALL IMAGES ************* #
    i = 0
    ierr = 0
    ithumb = 0
    images = []
    print("...processing the images\n\n")
    for filename in image_paths: 
        i += 1
        
        # every 10 images, print a counter
        # weird code block => the ':' will always be at the same position in a console
        if (i % 10 == 0):
            print(i
                  , ''.join(" " for i in range( 4 - len(str(i) 
                                                if len(str(i)) < 4 else "1")))
                  , ": "
                  , filename)
        
        # if the image a thumbnail (ends with `_thumbnail.{extension}`, don't
        # process it and move it to the `_thumbail` directory
        if "_thumbnail" in filename:
            os.rename( filename, os.path.join(thumbnail, os.path.basename(filename)) )
            image_paths_kept.remove(filename)
            ithumb += 1
            continue
            
        # clip preprocess + convert to rgb. 
        # if there's an error, move to `_unprocessable/` folder
        try:
            image = Image.open(filename).convert("RGB")
            images.append(preprocess(image))
        except (
            UnidentifiedImageError    # image is corrupt
            , DecompressionBombError  # image too big
            , OSError                 # image is truncated, whatever that means
        ):
            print(
                "\n     **** WARNING ~ unprocessable or corrupt image: "
                + f"\n         {filename}."
                + "\n     **** skipping the processing and moving image to `_unprocessable` \n"
            )
            os.rename( filename, os.path.join(unprocessed, os.path.basename(filename)) )
            image_paths_kept.remove(filename)
            ierr += 1
        except IsADirectoryError:
            image_paths_kept.remove(filename)
    
    # ************* END ************* #
    print ("\n\n...buiding the torch tensor")
    image_input = torch.tensor(np.stack(images))
    
    print ("...now generating the image features")
    with torch.no_grad():
        image_features = model.encode_image(image_input).float()
        image_features /= image_features.norm(dim=-1, keepdim=True)
    
    output = os.path.join(UTILS, f"{folder}_torch.pt")
    torch.save(image_features, output)
    
    # save the files that were actually processed to `directory_file`
    with open(directory_file, mode="w") as fh:
        fh.write("".join( f"{i}\n" for i in image_paths_kept ))
    
    print ("\n\n--> embeddings saved in %s" % output)
    print(f"--> model classification finished on {image_count} images")
    print("--> {ierr} images were unprocessable (corrupt or too large for PIL)")
    print(f"    those files were moved to {unprocessed}.")
    print(f"--> {ithumb} thumbnail files were also not processed by the model")
    print(f"    those files were moved to {thumbnail}")
    print(f"--> list of processed files saved to {directory_file}")
    return


