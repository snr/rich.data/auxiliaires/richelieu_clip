# generates a list of image files contained in a folder and its subfolders
# input: root folder
# output: text file

import os

from .utils.constants import STATICS


def recurse(walk_dir):
    """
    list all files recursively in the `walk_dir` provieded
    by the user. create two files from this:
    * `{walk_dir}.txt`: the paths to all images to process, with 1 image / line
    * `{walk_dir}_directory.txt`: a log file which seems to be used by pytorch
    
    :param walk_dir: the directory to list contents from, 
                     expressed as a relative path from the 
                     `statics` directory
    """
    rel_walk_dir = walk_dir                                            # relative path to `walk_dir`
    walk_dir = os.path.join(STATICS, walk_dir)                         # abspath to walkdir
    list_file = os.path.join(STATICS, f"{rel_walk_dir}.txt")           # file which stores path to 1 image per line
    log_file = os.path.join(STATICS, f"{rel_walk_dir}_directory.txt")  # log file used by torch (i think)
    
    # wipe previous contents from `log_file` and `list_file`
    open(log_file, mode="w").close()
    open(list_file, mode="w").close()
    
    log_writer = open(log_file, mode="a")
    list_writer = open(list_file, mode="a")

    log_writer.write('walk_dir = ' + rel_walk_dir + "\n")
    log_writer.write('walk_dir (absolute) = ' + walk_dir + "\n")
    
    for root, subdirs, files in os.walk(walk_dir):
        # process root
        log_writer.write('--\nroot = ' + root + "\n")
        
        # process subdir
        for subdir in subdirs:
            log_writer.write('\t- subdirectory ' + subdir + "\n")
        
        # process files
        i=1
        for filename in [ 
            filename for filename in files 
            if (
                filename.endswith(".jpg") 
                or filename.endswith(".jpeg") 
                or filename.endswith(".JPG") 
                or filename.endswith(".png")
            )
        ]:
            file_path = os.path.join(root, filename)
            if (i % 10 == 0):
                log_writer.write( f"\t-{str(i)} : {filename} (full path: {file_path})\n" )
            list_writer.write(f"{file_path}\n")
            i+=1
        
        log_writer.write(str(i) + "\n")

    log_writer.close()
    list_writer.close()
    return



